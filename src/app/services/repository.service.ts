import { Injectable } from '@angular/core';
import { Bibentry } from '../models/bibentry.model';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

import { timer } from 'rxjs/observable/timer';
import 'rxjs/add/operator/map';

@Injectable()
export class RepositoryService {

  private books : Bibentry[];

  constructor() {
    this.books = new Array<Bibentry>();

    let wmd : Bibentry  = new Bibentry();
    wmd.title = "Weapons of Math Destruction";
    wmd.authorFirst = 'cathy';
    wmd.authorLast = "O'Neil";
    wmd.year = 2016;
    wmd.publisher = "Broadway Books";
    wmd.id = 1;

    let angular : Bibentry = new Bibentry();
    angular.title = "Pro Angular";
    angular.authorFirst = 'Adam';
    angular.authorLast = "Freeman";
    angular.year = 2017;
    angular.publisher = "Apress";
    angular.id = 2;

    let cnh : Bibentry = new Bibentry();
    cnh.authorFirst = 'Bill';
    cnh.authorLast = 'Watterson';
    cnh.title = 'Calvin & Hobbes';
    cnh.year = 1987;
    cnh.publisher = 'Andrews McMeel Publishing';
    cnh.id = 3;

    this.books.push(cnh);
    this.books.push(wmd);
    this.books.push(angular);
  }

  get allBooks () : Observable<Bibentry[]> {
    return of(this.books);
  }

  addBook(book : Bibentry) : void {
    let currentMax : Bibentry = this.books[0]
    currentMax = this.books.reduce ((currentMax, element) => element.id > currentMax.id ? element : currentMax );
    book.id = currentMax.id + 1;
    console.log(`New book's ID set to ${book.id}`);
    this.books.push(book);
  }

  getBookById(id : number) : Observable<Bibentry> {
    let book : Bibentry = this.books.find(b => b.id === id);
    return of(book);
  }

  updateBook (book: Bibentry) : Observable<boolean> {
    //  Create a timer that will go off after 1 second
    const source = timer(1000);

    //  The timer gives us a single value of 0, which we'll map to true or false depending
    //  on a special value for the title
    return source.map (
      val => book.title === 'FAIL' ? false : true
    );
  }
}
